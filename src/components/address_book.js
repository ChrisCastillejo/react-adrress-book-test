import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { Grid } from "semantic-ui-react";
import styles from "../assets/styles";
import {
  fetchContactList,
  addContact,
  editContact,
  deleteContact
} from "../actions";
import AddressBookListView from "./address_book_list_view";
import AddressBookDetailView from "./address_book_detail_view";
import AddressBookEditView from "./address_book_edit_view";
class AddressBook extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contactList: [],
      selectedContact: {},
      view: "detail"
    };
    this.props.fetchContactList();
    this.selectContact = this.selectContact.bind(this);
    this.handleView = this.handleView.bind(this);
    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.saveContact = this.saveContact.bind(this);
    this.editContact = this.editContact.bind(this);
    this.deleteContact = this.deleteContact.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.props.contactList, nextProps.contactList)) {
      this.setState({
        contactList: nextProps.contactList,
        selectedContact:
          this.state.view === "detail"
            ? _.orderBy(nextProps.contactList, ["firstName"], ["asd"])[0]
            : this.state.selectedContact
      });
    }
  }

  selectContact(selectedContact) {
    this.setState({ selectedContact, view: "detail" });
  }
  checkEmail(selectedContact) {
    let errors = {};
    if (_.isEmpty(selectedContact.firstName)) {
      errors.firstName = true;
    }
    if (_.isEmpty(selectedContact.lastName)) {
      errors.firstName = true;
    }
    if (_.isEmpty(selectedContact.email)) {
      errors.firstName = true;
    }
    if (_.isEmpty(selectedContact.country)) {
      errors.firstName = true;
    }
    if (
      !/^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/.test(selectedContact.email)
    ) {
      errors.wrongEmail = true;
    }
    return errors;
  }

  saveContact(contact) {
    debugger;
    let errors = {};
    errors = this.checkEmail(contact);
    if (_.isEmpty(errors)) {
      if (this.state.view === "add") {
        contact.id =
          this.state.contactList[this.state.contactList.length - 1].id + 1;
        this.setState({
          message: false,
          view: "detail"
        });
        this.props.addContact(contact);
      }
    } else {
      if (errors.wrongEmail) {
        this.setState({ message: "Please insert a valid email address." });
      } else {
        this.setState({ message: "Please fill out all the fields." });
      }
    }
  }

  editContact(selectedContact) {
    let errors = {};
    errors = this.checkEmail(selectedContact);
    if (_.isEmpty(errors)) {
      this.props.editContact(selectedContact);
      this.setState({ view: "detail", errors: {}, selectedContact });
    } else {
      if (errors.wrongEmail) {
        this.setState({ message: "Please insert a valid email address." });
      } else {
        this.setState({ message: "Please fill out all the fields." });
      }
    }
  }

  deleteContact(selectedContact) {
    this.props.deleteContact(selectedContact);
    this.setState({ view: "detail" });
  }

  handleView(view) {
    let selectedContact = this.state.selectedContact;
    if (view === "add") {
      selectedContact = {
        firstName: "",
        lastName: "",
        email: "",
        country: ""
      };
    }
    this.setState({ view, selectedContact, message: false });
  }

  handleFieldChange(field, value) {
    if (field === "firstName" || field === "lastName") {
      value = value.charAt(0).toUpperCase() + value.slice(1);
    }
    this.setState({
      selectedContact: Object.assign({}, this.state.selectedContact, {
        [field]: value
      })
    });
  }
  render() {
    return (
      <div style={styles.background}>
        <Grid>
          <Grid.Row columns={2}>
            <AddressBookListView
              contactList={this.state.contactList}
              selectContact={this.selectContact}
              selectedContact={this.state.selectedContact}
              handleView={this.handleView}
              view={this.state.view}
              handleFieldChange={this.handleFieldChange}
            />
            {this.state.view === "add" || this.state.view === "edit" ? (
              <AddressBookEditView
                contactList={this.state.contactList}
                selectContact={this.selectContact}
                selectedContact={this.state.selectedContact}
                handleView={this.handleView}
                view={this.state.view}
                handleFieldChange={this.handleFieldChange}
                saveContact={this.saveContact}
                message={this.state.message}
                editContact={this.editContact}
                deleteContact={this.deleteContact}
              />
            ) : (
              <AddressBookDetailView
                contactList={this.state.contactList}
                selectContact={this.selectContact}
                selectedContact={this.state.selectedContact}
                handleView={this.handleView}
                view={this.state.view}
                handleFieldChange={this.handleFieldChange}
              />
            )}
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = ({ contactList }) => ({
  contactList
});

const actions = {
  fetchContactList,
  addContact,
  editContact,
  deleteContact
};

export default connect(mapStateToProps, actions)(AddressBook);
