import {
  FETCH_CONTACT_LIST,
  ADD_CONTACT,
  EDIT_CONTACT,
  DELETE_CONTACT
} from "../actions/action_types";

export function contactReducer(state = [], action) {
  switch (action.type) {
    case FETCH_CONTACT_LIST:
    case ADD_CONTACT:
    case EDIT_CONTACT:
    case DELETE_CONTACT: {
      return action.payload;
    }
    default: {
      return state;
    }
  }
}
