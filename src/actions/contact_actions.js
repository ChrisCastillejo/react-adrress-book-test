import {
  FETCH_CONTACT_LIST,
  ADD_CONTACT,
  EDIT_CONTACT,
  DELETE_CONTACT
} from "./action_types";
import _ from "lodash";
import { DefaultContactList } from "../assets/data";

export function fetchContactList() {
  if (!localStorage.getItem("contactList")) {
    localStorage.setItem("contactList", JSON.stringify(DefaultContactList));
  }
  return dispatch =>
    dispatch({
      type: FETCH_CONTACT_LIST,
      payload: JSON.parse(
        localStorage.getItem("contactList").replace(/'/g, '"')
      )
    });
}

export function addContact(contact) {
  localStorage.setItem(
    "contactList",
    JSON.stringify([
      ...JSON.parse(localStorage.getItem("contactList").replace(/'/g, '"')),
      contact
    ])
  );
  return dispatch =>
    dispatch({
      type: ADD_CONTACT,
      payload: JSON.parse(
        localStorage.getItem("contactList").replace(/'/g, '"')
      )
    });
}

export function editContact(contact) {
  let contactList = JSON.parse(
    localStorage.getItem("contactList").replace(/'/g, '"')
  );
  contactList[_.findIndex(contactList, { id: contact.id })] = contact;
  localStorage.setItem("contactList", JSON.stringify(contactList));
  return dispatch =>
    dispatch({
      type: EDIT_CONTACT,
      payload: JSON.parse(
        localStorage.getItem("contactList").replace(/'/g, '"')
      )
    });
}

export function deleteContact(contact) {
  let contactList = JSON.parse(
    localStorage.getItem("contactList").replace(/'/g, '"')
  );
  contactList.splice(
    _.findIndex(
      JSON.parse(localStorage.getItem("contactList").replace(/'/g, '"')),
      { id: contact.id }
    ),
    1
  );

  localStorage.setItem("contactList", JSON.stringify(contactList));
  return dispatch =>
    dispatch({
      type: DELETE_CONTACT,
      payload: JSON.parse(
        localStorage.getItem("contactList").replace(/'/g, '"')
      )
    });
}
