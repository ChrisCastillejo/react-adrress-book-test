export default {
  background: {
    padding: 30,
    color: "#676767"
  },
  editButton: {
    right: 19,
    backgroundColor: "#b0b0b0",
    color: "#fff",
    position: "absolute",
    boxShadow: "0px 1px 2px 2px #eeeeee"
  },
  detailTitle: { fontSize: 25, color: "#fff" },
  bigCircle: {
    height: 100,
    width: 100,
    backgroundColor: "#676767",
    color: "#fff",
    boxShadow: "0px 1px 3px 3px #eeeeee"
  },
  pBigCircle: { fontSize: 43, marginTop: 7 },
  detailName: { fontSize: 35, marginTop: 10, marginBottom: -20 },
  detailGrid: { padding: 40, textAlign: "left", fontSize: 20 },
  detailField: { marginTop: -20, fontSize: 23, fontWeight: "bold" },
  detailRightColum: { paddingLeft: 70 },
  checkButton: {
    right: 15,
    backgroundColor: "#4cd964",
    color: "#fff",
    position: "absolute",
    boxShadow: "0px 1px 2px 2px #eeeeee"
  },
  editOrAddButton: { fontSize: 25 },
  editInput: {
    marginTop: -20,
    marginBottom: 15,
    width: 200,
    fontSize: 15
  },
  errors: { marginTop: -20, color: "red" },
  deleteButton: {
    right: 15,
    bottom: 0,
    backgroundColor: "#ff3b30",
    color: "#fff",
    position: "absolute",
    boxShadow: "0px 1px 2px 2px #eeeeee"
  },
  contactListTitle: { fontSize: 40, fontWeight: "bold" },
  addButton: {
    backgroundColor: "#b0b0b0",
    color: "#fff",
    boxShadow: "0px 1px 2px 2px #eeeeee"
  },
  contactList: { overflow: "auto", height: 0.7 * window.innerHeight },
  pList: { fontSize: 16 },
  bold: { fontWeight: "bold" },
  cursor: { cursor: "pointer" },
  dropdownEdit: {
    marginTop: -10,
    marginBottom: 15,
    width: 200,
    fontSize: 15
  }
};
